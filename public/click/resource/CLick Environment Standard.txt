The CLick Environment Standard
version 0.0.3
Copyright (c) 2020, Daniel (Robin) Smith, smithtrombone@gmail.com


1 Introduction..................................................................

The CLick Environment Standard describes methods that ANSI Lisp implementations
can use to increase compatibility and portability of compiled code and
native binary objects.



1.1 Purpose.....................................................................

The purpose of this document is to outline the requirements for implementing a
modern, viable, and standalone runtime environment and toolchain for the ANSI
Common Lisp language standard.

The CLick Environment Standard serves as a description of how to address the
"implmentation dependent" sections of the ANSI Lisp Standard in a common manner.



1.2 Compatibility...............................................................

The CLick Environment Standard assumes the host system contains an ANSI C++
Standard compliant compiler toolchain.

This standard also assumes that the host system is mostly POSIX compliant.
"mostly" is used to describe compliance because many systems are compliant but
are not officially verified (i.e. systems that follow the Linux Standard
Base). The most basic assumption is that the host system will have an
implementation of the POSIX Standard C libraries.

This Standard assumes the host system uses a 64 bit architecture. It may be
possible for implementations to conform to this Standard using a 32 bit
architecture.

Because ANSI Lisp makes an aggressive point of being future-proof, it should be
pointed out that this Standard is only compatible with computing machines that
implement the Von Neumann model.



1.3 Rationale...................................................................

There are many implementations of the ANSI Lisp Standard and each one has
different behavior beyond the Lisp environment. This makes it difficult to
provide code libraries that will work across all implementations. Each
implementation uses exotic methods to generate executables from Lisp code. These
executables are delicate, inefficient, and largely non-portable. Also, because
all operating systems available are written in the C language and exclusively
offer C language libraries for communicating with the system, Lisp requires
foreign function interfaces to achieve functionality that is normal for modern
software applications. This causes a loss in application performance.

These issues prevent ANSI Lisp from being the first choice even when the
language is the most applicable to the problem.

CLick tries to address these problems by existing as a standard, rather than
an implementation. This solves the problem of incompatibility between
implementations: Any implementation can add a set of features and become
compatible with the CLick Environment Standard. CLick defines a specific method
to generate executable and library binary objects from Lisp code. CLick details
a RISC bytecode to represent code compiled by a Common Lisp implementation.
CLick describes a set of pre-defined Common Lisp packages that allow direct
access to system resources. CLick also defines a method to integrate foreign
language libraries into the environment without needing a foreign function
interface to match the type system.



2 - The CLick Environment.......................................................

The CLick Environment provides the host system with a toolchain and a runtime
environment for executable and library objects created with the ANSI Lisp
language.



2.1 - Core Features.............................................................

The CLick Core Features are the minimum requirement for an implementation to be
compliant with this Standard. These requirements allow an implementation to
support the execution of native binary objects created in the CLick environment.

The Core Features include applications and utilities to facilitate a basic
execution environment. The section also includes definitions and systems that
a compliant implementation must support.



2.1.1 - The CLick Dynamic Linker/Loader.........................................

Some systems will have compatible architectures and kernels, but different
operating system features. This results in the location of each system's
linker/loader utility not being guaranteed. This prevents a native object
created on one host system from being executed on a compatible target system
with a linker in a different location than the host system.

The CLick Linker/Loader guarantees the existence and location of a linker for
any application that is compatible. The CLick Linker/Loader will always have the
filename "CL" and it will always be located in the directory "/usr/lib/CLick".
Whenever a native binary object is created using CLick, it should be linked
using "CL" rather than the host system's native linker/loader.

The CLick Linker/Loader is capable of loading libraries formatted by the CLick
Environment as well as other environments. This allows applications that have
been linked to foreign code libraries to function properly.



2.1.2 - The CLick Bytecode Instruction Set .....................................

CLick Bytecode will be used by compliant Common Lisp implementations to
represent internal and external compiled code.

The CLick Bytecode Instruction Set is described in detail in the CLick Bytecode
Instruction Set Reference, a companion document to this standard.



2.1.2 - The CLick Application Binary Interface..................................

Compiled code objects created by compliant implementations use the CLick ABI to
define compiled functions. The CLick ABI is used by the REPL and by the CLick
The CLick Application Binary Interface is used with compiled code objects
produced using the CLick Bytecode Instruction Set. The CLick ABI is used by the
REPL and by the CLick Linker to read compiled code objects.

The CLick Application Binary Interface is described in detail in the CLick
Application Binary Interface Reference, a companion document to this standard.


2.1.3 - Binary Object Formats...................................................

This Standard currently supports three binary object formats: ELF, MACH-O, and
PE. These three formats can be used by compatible systems as either executables
or libraries. The three formats were chosen because they provide the greatest
range of compatibility across multiple types of host systems.

PE is a Microsoft binary object format. Microsoft is not itself mostly POSIX
compliant, but it recently has offered its users the Windows Subsystem for Linux
which allows users to run a Linux kernel in parallel to the Windows operating
system. Users should be able to use a conforming implementation with the
Windows Subsystem for Linux, but this is not guaranteed. Even if the Windows
Subsystem for Linux is not compatible with POSIX software, this standard will
support a PE format for cross-compilation with Microsoft Windows as a target
system.



2.1.4 - The CLick ANSI Common Lisp Implementation...............................

This section outlines the properties a conforming ANSI Common Lisp
implementation should possess.

A conforming implementation should implement a virtual machine that can
execute CLick Bytecode instructions. The implementation will use this virtual
machine to expose system resources using specific calls defined in the CLick
Bytecode Instruction Set. Any compiled function must be represented by CLick
bytecode and be executed using the virtual machine. If a conforming
implementation does not immediately compile defined functions, it shall be
capable of changing the execution flow between the virtual machine and the
abstract syntax representation as compiled and non-compiled functions are
called.

A conforming implementation should represent compiled files as CLick Bytecode
objects using the CLick ABI.

A conforming implementation will expose system resources using the packages
defined in Section 5. These packages shall be implemented using the CLick
A conforming implementation will use direct system calls when executing code
inside the virtual machine. It will not use the C language wrappers for those
system calls.

2.1.4.1 - The Editor............................................................
A conforming implementation must provide some means of text editing when the
"ed" function is invoked. The editor must be able to load and save a text file.
The editor must also have some means of submitting its text buffer back to the
REPL for reading and evaluation.



2.2 - Additional Features.......................................................
The features specified in this section are required for a complete
implementation of the CLick Environment Standard. They aren't necessary for
creation and execution of simple binary object files in a minimal environment,
but they are important to have for modern



2.2.1 - The CLick Integrator....................................................

The CLick integrator allows foreign language libraries to be included in the
CLick environment. The CLick integrator is a utility that parses a native
library object in one of the supported binary object formats and generates a
profile for use by the conforming Common Lisp implementation as well as the
CLik Linker.



2.2.2 - The CLick Assembler/Linker..............................................

The CLick Assembler/Linker is a compiler utility that takes a series of
compiled code objects created by the Common Lisp implementation and converts
them into native binary objects.

This is a command line utility that takes three arguments, the format of the
native binary object, the entry point if the object is an executable, and an
arbitrarily long list of the compiled object files to compile.

There is a limited set of acceptable inputs for the first argument:
"e" - signifies an ELF shared library object.
"m" - signifies a MACH-O shared library object.
"p" - signifies a PE shared library object.
"E" - signifies an ELF executable object.
"M" - signifies a MACH-O executable object.
"P" - signifies a PE executable object.

The second argument is only used if the first argument is "E", "M", or "P". The
second argument must be the name of a valid function as it would appear in the
REPL if the object file(s) were loaded.

If one of the objects uses an integrated library, it will create an object that
is dynamically linked to that library.



2.2.3 - The CLick Standard Packages.............................................

The Standard Packages define functions that allow the Common Lisp language to
access system resources like threading or networking. These packages are defined
in section 5.



3 - The CLick Bytecode Instruction Set..........................................

The CLick Bytecode Instruction Set defines a series of binary instructions for
performing lower level operations closer in semblance to assembly. This,
combined with the virtual machine inside conforming implementations allows
compiled functions to execute with greater efficiency. The bytecode also
posesses functionality to access low level system resources.



4 - The CLick Application Binary Interface......................................

The CLick ABI pertains only to compiled code objects represented with CLick
bytecode. This ABI allows the REPL and the CLick Linker to


5 - The CLick Standard Packages.................................................

The CLick Standard Packages define packages designed to make system resources
available to the ANSI Common Lisp language. They are not extensions to the
language. They are implemented using the CLick Bytecode Instruction Set in
order to perform special calls to the virtual machine as a way of
interfacing with the host system.
